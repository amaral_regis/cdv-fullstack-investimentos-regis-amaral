<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to('/clients')->send();
})->name('home');

//ROTAS PARA CONTROLLER CLIENT

Route::get('/clients', 'ClientController@index')->name('listclients');
Route::get('/client/create', 'ClientController@create')->name('createclient');
Route::post('/client/store', 'ClientController@store')->name('storeclient');
Route::get('/client/edit/{id}', 'ClientController@edit')->name('editclient');
Route::post('/client/update', 'ClientController@update')->name('updateclient');
Route::post('/client/{id}/delete', 'ClientController@destroy')->name('deleteclient');
Route::get('/client/{id}/{date?}', 'ClientController@show')->name('showclient');

//ROTAS PARA CONTROLLER PORTFOLIO
Route::get('/portfolio/create/{client_id}', 'PortfolioController@create')->name('createportfolio');
Route::post('/portfolio/store', 'PortfolioController@store')->name('storeportfolio');
Route::post('/portfolio/{id}/delete', 'PortfolioController@destroy')->name('deleteportfolio');
Route::get('/portfolio/edit/{id}', 'PortfolioController@edit')->name('editportfolio');
Route::post('/portfolio/update', 'PortfolioController@update')->name('updateportfolio');

//ROTAS PARA MOVIMENTAÇÕES DE ATIVOS
Route::get('/movimentasset/create/{client_id}/{portfolio_id}', 'MovimentAssetController@create')->name('createmovimentasset');
Route::post('/movimentasset/store', 'MovimentAssetController@store')->name('storemovimentasset');
Route::get('/movimentasset/edit/{client_id}/{portfolio_id}/{moviment_id}', 'MovimentAssetController@edit')->name('editmovimentasset');
Route::post('/movimentasset/update', 'MovimentAssetController@update')->name('updatemovimentasset');
Route::post('/movimentasset/{client_id}/{portfolio_id}/delete', 'MovimentAssetController@destroy')->name('deletemovimentasset');

//IMPORTAÇÃO
Route::get('/importdata', 'ImportDataController@index')->name('importdata');
Route::post('/uploadfile', 'ImportDataController@uploadfile')->name('uploadfile');

//API
Route::get('/api/{client_id?}', 'ApiController@index')->name('api');
