<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovimentAsset extends Model
{
    protected $fillable = [
        'id',
        'date',
        'operation',
        'price',
        'amount',
        'asset_symbol',
        'asset_name',
        'asset_class',
        'portfolio_id',
        'portfolio_client_id'
    ];

    public function client()
    {
        return $this->belongsTo('App\Models\Client','portfolio_client_id');
    }

    public function portfolio()
    {
        return $this->belongsTo('App\Models\Portfolio','portfolio_id');
    }
}
