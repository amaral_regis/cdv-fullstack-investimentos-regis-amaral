<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'id',
        'name'
    ];

    public function portfolios()
    {
        return $this->hasMany('App\Models\Portfolio','client_id');
    }

    public function movimentassets()
    {
        return $this->hasMany('App\Models\MovimentAsset','portfolio_client_id');
    }
}
