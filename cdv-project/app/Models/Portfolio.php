<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $fillable = [
        'id',
        'name',
        'client_id'
    ];

    public function movimentassets()
    {
        return $this->hasMany('App\Models\MovimentAsset','portfolio_id');
    }
}
