<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Models\Client;
use App\Models\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{

    public function create($client_id)
    {
        $Client = Client::find($client_id);
        if(!isset($Client)){
            return redirect()->route('listclients')->with('error', "Cliente não encontrado.");
        }
        return view('Portfolio.create')->with('Client', $Client);
    }

    public function store(Request $request)
    {
        if(!isset($request->client_id))
            return redirect()->back()->with('error', 'Cliente não informado.');

        if(!isset($request->name))
            return redirect()->back()->with('error', 'Informe um nome para o portfolio!');

        $Client = Client::find($request->client_id);
        if(!isset($Client)){
            dd($Client);
            return redirect()->back()->with('error', "Cliente não encontrado.");
        }

        $Portfolio = new Portfolio;
        $Portfolio->name = $request->name;
        $Portfolio->client_id = $Client->id;

        try {
            $Portfolio->save();
        }catch (\Exception $e){
            if(env('APP_DEBUG') == true){
                return redirect()->back()->with('error', $e->getMessage());
            }else{
                return redirect()->back()->with('error', "Não foi possível completar a requisição.");
            }
        }
        return redirect()->route('showclient', $Client->id)->with('success', "Portfólio criado.");
    }

    public function edit($portfolio_id)
    {
        $Portfolio = Portfolio::find($portfolio_id);
        if(!isset($Portfolio)){
            return redirect()->back()->with('error', "Portfólio não encontrado.");
        }
        return view('Portfolio.edit')->with('Portfolio',$Portfolio);
    }

    public function update(Request $request)
    {
        $Portfolio = Portfolio::find($request->id);
        if(!isset($Portfolio)){
            return redirect()->back()->with('error', "Portfólio não encontrado.");
        }

        if(!isset($request->name)) {
            return redirect()->route('editportfolio', $Portfolio->id)->with('error', 'Informe um nome para o portfolio!');
        }

        $Portfolio->name = $request->name;
        try {
            $Portfolio->save();
        }catch (\Exception $e){
            if(env('APP_DEBUG') == true){
                return redirect()->back()->with('error', $e->getMessage());
            }else{
                return redirect()->back()->with('error', "Não foi possível completar a requisição.");
            }
        }
        return redirect()->route('showclient', $Portfolio->client_id)->with('success', "Nome do portfólio atualizado.");
    }

    public function destroy(Request $request)
    {
        $Portfolio = Portfolio::find($request->id);
        $client_id = $Portfolio->client_id;

        try {
            $Portfolio->delete();
        }catch (\Exception $e){
            if(env('APP_DEBUG') == true){
                return redirect()->back()->with('error', $e->getMessage());
            }else{
                return redirect()->back()->with('error', "Não foi possível completar a requisição.");
            }
        }
        return redirect()->route('showclient', $client_id)->with('success', "Portfólio deletado.");
    }
}
