<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Http\CsvImportRequest;
use App\Models\Client;
use App\Models\MovimentAsset;
use App\Models\Portfolio;
use Carbon\Carbon;

class ImportDataController extends Controller
{
    public function index()
    {
        return view('importdata');
    }

    public function uploadfile(CsvImportRequest $request)
    {
        //recebo o arquivo e crio o array com os dados
        $path = $request->file('csv_file')->getRealPath();
        $data = array_map('str_getcsv', file($path));
        //ignoro o cabeçalho
        $csv_data = array_splice($data, 1);

        $data_total = count($csv_data);
        $ignoreds = 0;
        $errors = 0;

        //percorro cada um dos registro
        foreach ($csv_data as $d){
            //converto a data para um formato compatível
            $date = Carbon::createFromFormat('d/m/y',  $d[0])->format('Y-m-d');
            $name = $d[1];
            $asset_symbol = strtoupper($d[2]);
            //compra = 1 (true) / venda = 0 (false)
            $moviment_asset_operation = strtolower($d[3]) == 'venda' ? 0 : (strtolower($d[3]) == 'compra' ? 1 : null);
            $moviment_asset_amount = $d[4];
            $moviment_asset_price = str_replace(',', '.', $d[5]);

            $Client = Client::where('name', $name)->first();

            //se o cliente não existe ignoro e passo para o próximo registro
            if(!isset($Client)){
                $ignoreds++;
                continue;
            }

            //nova movimentação de ativos
            $MovimentAsset = new MovimentAsset;

            //se o cliente já possui um portfólio uso o existente
            if(isset($Client->portfolios) && count($Client->portfolios) > 0){
                $MovimentAsset->portfolio_id = $Client->portfolios[0]->id;
            }else{
                //se não crio um com o nome Carteira CDV
                $Portifolio = new Portfolio;
                $Portifolio->client_id = $Client->id;
                $Portifolio->name = 'Carteira CDV';
                $Portifolio->save();
                $MovimentAsset->portfolio_id = $Portifolio->id;
            }

            //configuro os parametros restantes da movimentação
            $MovimentAsset->portfolio_client_id = $Client->id;
            $MovimentAsset->date = $date;
            $MovimentAsset->operation = $moviment_asset_operation;
            $MovimentAsset->price = $moviment_asset_price;
            $MovimentAsset->amount = $moviment_asset_amount;
            $MovimentAsset->asset_symbol = $asset_symbol;

            //salvo
            try{
                $MovimentAsset->save();
            }catch (\Exception $e){
                $errors++;
            }
        }
        return redirect()->route('importdata')->with('success', "{$data_total} registros carregados, {$ignoreds} ignorados, {$errors} erros");
    }
}

