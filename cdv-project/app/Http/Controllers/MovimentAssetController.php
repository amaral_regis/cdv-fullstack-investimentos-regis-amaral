<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Models\Client;
use App\Models\Portfolio;
use App\Models\MovimentAsset;
use Illuminate\Http\Request;

class MovimentAssetController extends Controller
{

    public function create($client_id, $portfolio_id)
    {
        $Client = Client::find($client_id);
        if(!isset($Client)){
            return redirect()->route('listclients')->with('error', "Cliente não encontrado.");
        }

        if(!isset($Client->portfolios[0])){
            return redirect()->route('showclient', $Client->id)->with('error', "Portfólio não encontrado.");
        }

        return view('MovimentAsset.create')->with('Client', $Client);
    }

    public function store(Request $request)
    {
        if(is_null(Client::find($request->portfolio_client_id)))
            return redirect()->back()->with('error', 'Cliente não encontrado.');

        if(is_null(Portfolio::find($request->portfolio_id)))
            return redirect()->back()->with('error', 'Portfólio não encontrado.');

        $colluns = ['date', 'operation', 'price', 'amount', 'asset_symbol', 'asset_name', 'asset_class', 'portfolio_id', 'portfolio_client_id'];

        $MovimentAsset = new MovimentAsset;
        foreach ($colluns as $collun){
            if(!isset($request->{$collun})){
                return redirect()->back()->with('error', "Todos os campos devem ser preenchidos. ({$collun})");
            }else{
                $MovimentAsset->{$collun} = $request->{$collun};
            }
        }

        try {
            $MovimentAsset->save();
        }catch (\Exception $e){
            if(env('APP_DEBUG') == true){
                return redirect()->back()->with('error', $e->getMessage());
            }else{
                return redirect()->back()->with('error', "Não foi possível completar a requisição.");
            }
        }
        return redirect()->route('showclient', $MovimentAsset->portfolio_client_id)->with('success', "Nova movimentação de ativo inserida.");
    }

    public function edit($client_id, $portfolio_id, $moviment_id)
    {
        $MovimentAsset = MovimentAsset::where('portfolio_id', $portfolio_id)
            ->where('portfolio_client_id', $client_id)
            ->where('id', $moviment_id)
            ->first();
        if(!isset($MovimentAsset)){
            return redirect()->route('showclient',  $client_id)->with('error', "Movimento não encontrado.");
        }

        return view('MovimentAsset.edit')->with('MovimentAsset',$MovimentAsset);
    }

    public function update(Request $request)
    {
        $MovimentAsset = MovimentAsset::where('portfolio_id', $request->portfolio_id)
            ->where('portfolio_client_id', $request->portfolio_client_id)
            ->where('id', $request->id)
            ->first();
        if(!isset($MovimentAsset)){
            return redirect()->route('showclient',  $request->portfolio_client_id)->with('error', "Movimento não encontrado.");
        }

        $colluns = ['date', 'operation', 'price', 'amount', 'asset_symbol', 'asset_name', 'asset_class'];

        foreach ($colluns as $collun){
            if(!isset($request->{$collun})){
                return redirect()->back()->with('error', "Todos os campos devem ser preenchidos. ({$collun})");
            }else{
                $MovimentAsset->{$collun} = $request->{$collun};
            }
        }

        try {
            $MovimentAsset->save();
        }catch (\Exception $e){
            if(env('APP_DEBUG') == true){
                return redirect()->back()->with('error', $e->getMessage());
            }else{
                return redirect()->back()->with('error', "Não foi possível completar a requisição.");
            }
        }
        return redirect()->route('showclient', $MovimentAsset->client->id)->with('success', "Movimento atualizado.");
    }

    public function destroy(Request $request)
    {
        $MovimentAsset = MovimentAsset::where('portfolio_id', $request->portfolio_id)
            ->where('portfolio_client_id', $request->client_id)
            ->first();
        if(!isset($MovimentAsset)){
            return redirect()->route('showclient',  $request->client_id)->with('error', "Movimento não encontrado.");
        }
        try {
            $MovimentAsset->delete();
        }catch (\Exception $e){
            if(env('APP_DEBUG') == true){
                return redirect()->back()->with('error', $e->getMessage());
            }else{
                return redirect()->back()->with('error', "Não foi possível completar a requisição.");
            }
        }
        return redirect()->route('showclient', $request->client_id)->with('success', "Movimento deletado.");
    }
}
