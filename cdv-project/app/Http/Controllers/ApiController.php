<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Http\AssetPositions;
use App\Models\Client;
use App\Models\MovimentAsset;

class ApiController extends Controller
{
    /*
     * Fornece um JSON contendo as posições de um cliente específico ou de todos
     */
    public function index($clients_ids = null)
    {

        //busco todos os clientes ou clientes específicos através das ids que recebi como parametro separadas por vírgulas ex: 1,6,8
        $Clients = is_null($clients_ids) ? Client::all() : Client::findMany(explode(',', $clients_ids));

        //percorro cara cliente na lista
        foreach ($Clients as $index => $client){
            //busco seus registros de movimentações de ativos
            $MovimentAssets = MovimentAsset::where('portfolio_client_id', $client->id)
                ->orderBy('date', 'DESC')
                ->get();
            //consolido suas posições
            $client->positions = AssetPositions::getPositions($MovimentAssets);
        }
        //se tudo ocorreu bem envio os dados no formato JSON
        if(isset($Clients)){
            return response(json_encode($Clients, JSON_PRETTY_PRINT), 200,
                ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8', 'Access-Control-Allow-Origin' => '*'],
                JSON_UNESCAPED_UNICODE);
        }
    }
}

