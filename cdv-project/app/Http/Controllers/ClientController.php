<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Http\AssetPositions;
use App\Models\Client;
use App\Models\MovimentAsset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class ClientController extends Controller
{
    public function index()
    {
        $Clients = Client::all();
        return view('Client.index')->with('Clients',$Clients);
    }

    public function create()
    {
        return view('Client.create');
    }

    public function store(Request $request)
    {

        if(!isset($request->name))
            return redirect()->back()->with('error', 'Informe o nome do cliente!');
        $Client = new Client;
        $Client->name = $request->name;
        try {
            $Client->save();
        }catch (\Exception $e){
            if(env('APP_DEBUG') == true){
                return redirect()->back()->with('error', $e->getMessage());
            }else{
                return redirect()->back()->with('error', "Não foi possível completar a requisição.");
            }
        }
        return redirect()->route('showclient', $Client->id);

    }

    public function show($client_id, $dt = null)
    {
        $Client = Client::find($client_id);
        if(!isset($Client)){
            return redirect()->route('listclients')->with('error', "Cliente não encontrado.");
        }

        $date = is_null($dt) ? \Illuminate\Support\Carbon::now() : $dt;

        $MovimentAssets = MovimentAsset::where('portfolio_client_id', $Client->id)
            ->whereDate('date', '<=', $date)
            ->orderBy('date', 'DESC')
                ->get();

//        dd($MovimentAssets);

        $consolidado = AssetPositions::getPositions($MovimentAssets);

        return view('Client.show')->with(['Client' => $Client, 'MovimentAssets' => $MovimentAssets, 'date' => $date, 'consolidado' => $consolidado]);

    }

    public function edit($client_id)
    {
        $Client = Client::find($client_id);

        if(!isset($Client)){
            return redirect()->route('listclients')->with('error', "Cliente não encontrado.");
        }
        return view('Client.edit')->with('Client',$Client);
    }

    public function update(Request $request)
    {
        $Client = Client::find($request->id);
        if(!isset($Client)){
            return redirect()->route('listclients')->with('error', "Cliente não encontrado.");
        }
        if(!isset($request->name)){
            return redirect()->route('editclient', $Client->id)->with('error', "O campo nome deve ser preenchido.");
        }
        $Client->name = $request->name;
        try {
            $Client->save();
        }catch (\Exception $e){
            if(env('APP_DEBUG') == true){
                return redirect()->back()->with('error', $e->getMessage());
            }else{
                return redirect()->back()->with('error', "Não foi possível completar a requisição.");
            }
        }
        return redirect()->route('showclient', $Client->id)->with('success', "As informações do cliente foram atualizadas.");
    }

    public function destroy(Request $request)
    {
        $Client = Client::find($request->id);
        if(!isset($Client)){
            return redirect()->route('listclients')->with('error', "Cliente não encontrado.");
        }
        try {
            $Client->delete();
        }catch (\Exception $e){
            if(env('APP_DEBUG') == true){
                return redirect()->back()->with('error', $e->getMessage());
            }else{
                return redirect()->back()->with('error', "Não foi possível completar a requisição.");
            }
        }
        return redirect()->route('listclients')->with('success', "Cliente deletado.");
    }
}
