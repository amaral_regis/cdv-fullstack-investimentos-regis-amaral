<?php


namespace App\Http;


use App\Models\MovimentAsset;

class AssetPositions
{
    /*
     * CÁLCULO DO PREÇO MÉDIO
     *
     * array $consolidado (simbolo do ativo como chave) armazena o valor total das compras (total_compras),
     * o total comprado (qtd_comprada) e a quantidade atual em carteira (qtd_ativo)
     *
     * Preço Médio = total_compras / qtd_comprada
     */
    public static function getPositions($MovimentAssets)
    {

        $consolidado = null;

        //Para exibição a lista é ordenada de maneira decrescente,
        //logo percorro a lista de trás pra frente
        for ($i = count($MovimentAssets) - 1; $i >= 0; $i--) {

            //para simplificar a escrita apenas
            $MovAsset = $MovimentAssets[$i];

            //chave para o array $consolidado
            $symbol = $MovAsset->asset_symbol;

            //verifico se já há uma posição consolidada no array $consolidado
            if (isset($consolidado[$symbol])) {
                //compra
                if ($MovimentAssets[$i]->operation) {

                    $consolidado[$symbol]['qtd_comprada'] = $consolidado[$symbol]['qtd_comprada'] + $MovAsset->amount;
                    $consolidado[$symbol]['total_compras'] = $consolidado[$symbol]['total_compras'] + ($MovAsset->amount * $MovAsset->price);
                    $consolidado[$symbol]['qtd_ativo'] = $consolidado[$symbol]['qtd_ativo'] + $MovAsset->amount;

                } else {//venda

                    //atualizo a quantidade do ativo em carteira após uma venda
                    $consolidado[$symbol]['qtd_ativo'] = $consolidado[$symbol]['qtd_ativo'] - $MovAsset->amount;

                    //se a quantidade do ativo foi zerada, zero também as variáveis de cálulo do preço médio
                    if ($consolidado[$symbol]['qtd_ativo'] == 0) {
                        $consolidado[$symbol]['qtd_comprada'] = 0;
                        $consolidado[$symbol]['total_compras'] = 0;
                    }
                }
            } else {
                //se não existe crio uma posição inicial para uma operação de compra
                if ($MovAsset->operation) {
                    $consolidado[$symbol]['qtd_comprada'] = $MovAsset->amount;
                    $consolidado[$symbol]['total_compras'] = $MovAsset->amount * $MovAsset->price;
                    $consolidado[$symbol]['qtd_ativo'] = $MovAsset->amount;
                }

            }
        }
        return $consolidado;
    }
}
