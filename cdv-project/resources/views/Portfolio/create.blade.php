@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="title">Novo Portfólio </div>
        <br>
        <form action="{{ route('storeportfolio') }}" method="post">
            @csrf
            <div class="form-group">
                <input type="hidden" name="client_id" value="{{$Client->id}}">
                <input type="text" class="form-control" id="name" name="name" value="">
            </div>
            <button type="submit" class="btn btn-primary btn-xs">Salvar</button>
            <a href="{{route('showclient', $Client->id)}}" class="btn btn-primary btn-xs" title="Editar">Cancelar</a>
        </form>
    </div>
@endsection
