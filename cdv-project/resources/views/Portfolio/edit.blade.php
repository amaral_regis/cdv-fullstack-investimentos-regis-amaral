@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="title">Alterar Portfólio </div>
        <br>
        <form action="{{ route('updateportfolio', $Portfolio->id) }}" method="post">
            @csrf
            <div class="form-group">
                <input type="hidden" name="id" value="{{$Portfolio->id}}">
                <input type="text" class="form-control" id="name" name="name" placeholder="Digite um nome para o portfólio" value="{{$Portfolio->name}}" required>
            </div>
            <button type="submit" class="btn btn-primary btn-xs">Salvar</button>
            <a href="{{route('showclient', $Portfolio->client_id)}}" class="btn btn-primary btn-xs" title="Editar">Cancelar</a>
        </form>
    </div>
@endsection
