@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="title">Cliente {{$Client->name}}
            <a href="{{route('showclient', $Client->id)}}" class="btn btn-primary btn-xs" style="float: right">Cancelar</a>
        </div>
        <h3>Nova movimentação em {{$Client->portfolios[0]->name}}</h3>
        <form action="{{ route('storemovimentasset') }}" method="post">
        @csrf
            <input type="hidden" name="portfolio_id" value="{{$Client->portfolios[0]->id}}">
            <input type="hidden" name="portfolio_client_id" value="{{$Client->id}}">

            <div class="form-group row">
                <label for="date" class="col-sm-2 col-form-label">Data</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" id="date" name="date" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="operation" class="col-sm-2 col-form-label">Operação</label>
                <div class="col-sm-10">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" value="1" checked>
                        <label class="form-check-label" for="operation">Compra</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" value="0">
                        <label class="form-check-label" for="operation">Venda</label>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="price" class="col-sm-2 col-form-label">Preço</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <div class="input-group-addon">R$</div>
                        <input type="number" min="0.00" step="0.01" id="price" name="price" class="form-control" placeholder="0,00" required>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="amount" class="col-sm-2 col-form-label">Quantidade</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <div class="input-group-addon">UN</div>
                        <input type="number" min="1" step="1" id="amount" name="amount" class="form-control" placeholder="0" required>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="asset_symbol" class="col-sm-2 col-form-label">Símbolo do ativo</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="asset_symbol" name="asset_symbol" placeholder="Símbolo do ativo" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="asset_name" class="col-sm-2 col-form-label">Nome do ativo</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="asset_name" name="asset_name" placeholder="Nome do ativo" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="asset_class" class="col-sm-2 col-form-label">Classe do ativo</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="asset_class" name="asset_class" placeholder="Classe do ativo" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="submit_form" class="col-sm-2 col-form-label"> &nbsp;</label>
                <div class="col-sm-10">
                    <button class="btn btn-primary btn-xs" type="submit" id="submit_form">Inserir movimentação</button>
                </div>
            </div>
        </form>
    </div>
@endsection
