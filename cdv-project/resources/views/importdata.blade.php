@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="title">Importar Dados</div>
        <br>
        <br>
        <form action="{{ route('uploadfile') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <input class="form-control form-control-xs" id="csv_file" name="csv_file" type="file" accept=".csv" required/>
            </div>
            <button type="submit" class="btn btn-primary btn-xs">Importar</button>
        </form>
    </div>
@endsection
