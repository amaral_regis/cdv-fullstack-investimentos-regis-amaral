@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="title">Clientes
            <a href="{{route('createclient')}}" class="btn btn-primary btn-xs" style="float: right">Inserir novo cliente</a>
        </div>
        <br>
        <input class="form-control" id="ClientName" type="text" placeholder="Procurar...">
        <div class="list-group" id="ClientList" style="width: 100%">
            @foreach($Clients as $Client)
                <a href="{{route('showclient', $Client->id)}}" class="list-group-item list-group-item-action">{{$Client->name}}</a>
            @endforeach
        </div>

        <script type="application/javascript">
            $(document).ready(function() {
                $("#ClientName").on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $("#ClientList a").filter(function () {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>
    </div>
@endsection
