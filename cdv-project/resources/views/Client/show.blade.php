@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="title">Cliente: {{ $Client->name }}</div>
        <form action="{{route('deleteclient', $Client->id)}}" method="post" onsubmit="return confirm('O cliente e todos seus dados serão permanentemente deletados! Deseja continuar?')">
            @csrf
            <a href="{{route('editclient', $Client->id)}}" class="btn btn-primary btn-xs person" title="Editar Cliente">
                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
            <button type="submit" class="btn btn-danger btn-xs person" title="Excluir Cliente">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </button>
        </form>
    </div>
    <div class="row">
        @if(isset($Client->portfolios) && count($Client->portfolios) == 0)
            <div class="title">Portifólio
                <a href="{{route('createportfolio', $Client->id)}}" class="btn btn-primary btn-xs" style="float: right">Criar Portfólio</a>
            </div>
            <br>
            <div style="text-align: center; width: 100%;">Nenhum portfólio cadastrado</div>
        @else
            <div class="title">{{$Client->portfolios[0]->name}}
                <input title="Selecione uma data anterior" type="date" class="form-control btn-xs" onkeydown="return false" onchange="handler(event)"
                       id="date" name="date" style="float: right; font-size: 12px; height: 27px !important; width: 140px; display: inline;" value="{{\Carbon\Carbon::parse($date)->format('Y-m-d')}}">
            </div>

            <form action="{{route('deleteportfolio', $Client->portfolios[0]->id)}}" method="post" onsubmit="return confirm('O portfólio e suas movimentações serão permanentemente deletados! Deseja continuar?')">
                @csrf
                <a href="{{route('editportfolio', $Client->portfolios[0]->id)}}" class="btn btn-primary btn-xs person" title="Editar Portfólio">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                <button type="submit" class="btn btn-danger btn-xs person" title="Excluir Portfólio">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button>

            </form>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Ativo</th>
                        <th scope="col">Quantidade</th>
                        <th scope="col">Preço Médio</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($consolidado))
                        @php
                            $i = 0;
                        @endphp
                        @foreach($consolidado as $simbolo => $posicao)
                            @php
                                $i++;
                            @endphp
                            <tr>
                                <th scope="row">{{$i}}</th>
                                <td>{{$simbolo}}</td>
                                <td>{{$posicao['qtd_ativo']}}</td>
                                <td>{{\Akaunting\Money\Money::BRL((round($posicao['total_compras'] / $posicao['qtd_comprada'], 2)), true)}}</td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
    </div>

    <div class="row">
        <div class="title">Movimentações
            <a href="{{route('createmovimentasset', [$Client->id, $Client->portfolios[0]->id])}}" class="btn btn-primary btn-xs" style="float: right">Nova Movimentação</a>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Data</th>
                    <th scope="col">Operação</th>
                    <th scope="col">Ativo</th>
                    <th scope="col">Quantidade</th>
                    <th scope="col">Preço</th>
                    <th scope="col">Opções</th>
                </tr>
                </thead>
                <tbody>
                @for($i = 0; $i < count($MovimentAssets); $i++)
                    <tr>
                        <th scope="row">{{$i+1}}</th>
                        <td>{{ \Carbon\Carbon::parse($MovimentAssets[$i]->date)->format('d/m/Y')}}</td>
                        <td>{{($MovimentAssets[$i]->operation == 1 ? 'Compra' : 'Venda')}}</td>
                        <td>{{$MovimentAssets[$i]->asset_symbol}}</td>
                        <td>{{$MovimentAssets[$i]->amount}}</td>
                        <td>{{\Akaunting\Money\Money::BRL($MovimentAssets[$i]->price, true)}}</td>
                        <td>
                            <form action="{{route('deletemovimentasset', [$Client->id, $Client->portfolios[0]->id])}}" method="post" onsubmit="return confirm('O movimento será permanentemente deletado! Deseja continuar?')">
                                @csrf
                                <a href="{{route('editmovimentasset', [$Client->id, $Client->portfolios[0]->id, $MovimentAssets[$i]->id])}}" class="btn btn-primary btn-xs" title="Editar">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                <button type="submit" class="btn btn-danger btn-xs" title="Excluir">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endfor
                </tbody>
            </table>
        </div>
        @endif
    </div>
    <script>
        function handler(e) {
            window.location.href = '{{route('showclient', $Client->id)}}/' + e.target.value;
        }
    </script>
@endsection
