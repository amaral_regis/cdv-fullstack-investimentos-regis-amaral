@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="title">Editar Cliente <a href="{{route('listclients')}}" class="btn btn-primary btn-xs" style="float: right">Lista de Clientes</a></div>
        <br>
        <form action="{{ route('updateclient', $Client->id) }}" method="post">
            @csrf
            <div class="form-group">
                <input type="hidden" name="id" value="{{$Client->id}}">
                <input type="text" class="form-control" id="name" name="name" placeholder="Nome do cliente" value="{{$Client->name}}" required>
            </div>
            <button type="submit" class="btn btn-primary btn-xs">Salvar</button>
            <a href="{{route('showclient', $Client->id)}}" class="btn btn-primary btn-xs" title="Editar">Cancelar</a>
        </form>
    </div>
@endsection
