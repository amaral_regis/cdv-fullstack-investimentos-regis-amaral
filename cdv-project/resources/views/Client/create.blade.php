@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="title">Novo Cliente</div>
        <br>
        <form action="{{ route('storeclient') }}" method="post">
            @csrf
            <div class="form-group">
                <input type="text" class="form-control" id="name" name="name" placeholder="Digite o nome do cliente..." required>
            </div>
            <a href="{{route('listclients')}}" class="btn btn-primary btn-xs">Cancelar</a>
            <button type="submit" class="btn btn-primary btn-xs">Salvar</button>
        </form>
    </div>
@endsection
