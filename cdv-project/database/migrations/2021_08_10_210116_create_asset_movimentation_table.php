<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetMovimentationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moviment_assets', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date');
            $table->boolean('operation');
            $table->double('price', 8, 2);
            $table->double('amount', 8, 2);
            $table->string('asset_symbol');
            $table->string('asset_name')->nullable();
            $table->string('asset_class')->nullable();

            //RESTRIÇÕES DE INTEGRIDADE
            $table->foreignId('portfolio_id')
                ->constrained()
                ->onDelete('cascade');

            $table->foreignId('portfolio_client_id');
            $table->foreign('portfolio_client_id')->references('client_id')->on('portfolios');
            //FIM

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moviment_assets');
    }
}
