# Requisitos

- PHP 7.4.x - https://windows.php.net/download#php-7.4-nts-vc15-x64
- Composer - https://getcomposer.org/download
- Laravel 7 - Instalado via composer
- Mysql ou MariaDB - https://dev.mysql.com/downloads/installer
- Git
  
# Ferramentas utilizadas 
Apenas para fins de informação
- MySQL Workbench 8 - para modelagem do modelo do BD e acesso ao mesmo
- PHP Storm - IDE de desenvolvimento

# Instalação

Obs.: Esse passo a passo descreve meu ambiente de desenvolvimento no Windows 10, para Linux ou MAC adaptações deverão ser feitas.

Vamos lá...

1) Faça o download do pacote PHP 7.4.x e descompacte na raiz do disco local, exemplo: C:\PHP7.

2) Se não tiver um gerenciador de banco de dados, instale o MariaDB ou MySql (essa parte não tem muito mistério).
   
3) Instale o composer. Em algum momento durante a instalação ele vai pedir para localizar a instalação do PHP, basta selecionar a pasta extraída no C:\PHP7.

4) Abra um shell, navegue até o diretório onde quer executar o projeto e clone este repositório.

# Configurações do Ambiente

No php.ini descomente ou insira as seguintes linhas:

``` 
extension_dir="ext"
extension=curl
extension=mbstring
extension=openssl
extension=pdo_mysql
```

# Configurações da Aplicação

Após a instalação dos programas necessários e das configurações do ambiente, ainda é preciso realizar algumas configurações no projeto.

1) Crie uma base de dados com o nome que preferir, anote aí o nome para utilizar depois.

2) Anote também o usuário e senha que dá acesso a base de dados junto com o nome da base, ok?

3) Numa ide de sua preferência abra o projeto e dentro da pasta cdv-project procure pelo arquivo .env

4) Agora que já localizou o arquivo vamos editar ele.

5) Pra começar bonito vamos mudar o nome da aplicação, para isso na primeira linha do arquivo .env configure um valor para a variável APP_NAME:
```
APP_NAME="Clube do Valor"
```
6) Mais abaixo, procure pelas variáveis que começam com o prefixo DB, elas são usadas para configurar o acesso da aplicação a base de dados que você criou uns passos atrás...

7) Agora com as informações de conexão da base de dados (nome do banco, usuário e senha), defina os valores para as seguintes variáveis:
```
DB_DATABASE=nome_do_banco_de_dados
DB_USERNAME=nome_do_usuario_do_BD
DB_PASSWORD=senha_do_usuario_do_BD
```

8) Feitoria! Agora vamos rezar pra que a aplicação rode sem mais percalços!

9) Em um shell navegue para dentro da pasta cdv-project. Antes de rodar a aplicação vamos precisar carregar ferramentas adicionais da aplicação através do composer. Para isso rode o seguinte comando:

```
composer install
```

10) Agora vamos rodar as migrations, elas são arquivos do laravel responsáveis por criar as tabelas do banco de dados. Para criar as tabelas rode o seguinte comando:
```
php artisan migrate
```

11) Se tudo deu certo, e não apareceu nenhuma mensagem de erro é sinal que a aplicação se conectou com sucesso ao banco de dados, criou as tabelas e agora está tudo certo para rodar o projeto

12) Na mesma linha de comando anterior execute o seguinte comando:
```
php artisan serve
```

13) Não havendo nenhum erro vai aparecer o endereço que você pode utilizar para acessar a aplicação no navegador: http://127.0.0.1:8000

```
D:\Users\Regis\Desktop\CDV\cdv-project>php artisan serve
Laravel development server started: http://127.0.0.1:8000
[Thu Aug 12 20:13:12 2021] PHP 7.4.14 Development Server (http://127.0.0.1:8000) started
```
14) Se você conseguiu chegar até aqui sem me xingar por não estar usando container, meus parabéns! Fico te devendo um chopp! Agora só espero do fundo do coração que a aplicação se comporte da mesma maneira que se comporta aqui no meu desktop.




# O que foi feito:

- Gerenciamento de Clientes: listagem, exibição individual, inserção, alteração e exclusão;
- Gerenciamento de Portfólio: inserção, alteração e exclusão;
- Gerenciamento de Movimentação de Ativos: listagem, inserção, alteração e exclusão;
- Importação de movimentações através de arquivo CSV, ignorando o registro quando um cliente não existe no BD;
- Visualização do portfólio do cliente com todas as posições consolidadas exibindo preço médio, nome do ativo e quantidade;
- Visualização do portfólio do cliente em uma data passada;
- Chamada de Api retornando as posições consolidadas de um cliente específico, múltiplos clientes ou de todos os clientes;
- Restrição de integridade por chave estrangeira e "on delete cascade" no BD; 
- Uso do padrão MVC (o próprio framework utilizado já fornece a estrutura necessária);
- Padronização de nomes de métodos e variáveis em inglês:
  Obs.: a lingua portuguesa possui muitas flexões de linguagem tornando confusa a intenção de cada nomenclatura, também destoa com as nomenclaturas do php e do framework utilizado;
- Frontend adaptado para dispositivos móveis.



# O que não foi feito:
- Testes de unidade.

# Sugestões de melhorias

## 1 - Normalizar base de dados
O modelo inicial do BD criado no primeiro commit leva em consideração uma melhor categorização por classes de ativos (Ações, ETF, FII, ..) 
e redução da redundância de dados comuns a todos os clientes, como sigla e nome de um ativo. Isso reduziria possíveis erros de digitação ao possibilitar
o preenchimento de campos através de autocomplete ou select box. Também possibilitaria uma melhor consolidação de ativos por classe.

Obs.: Para cumprir com os requisitos do software dentro do prazo de entrega, escolhi por utilizar um BD sem normalização. A qualidade dessa metodologia é razóavel e não compromete a confiabilidade ou integridade dos dados.

## 2 - Limitar inserção de movimentação a data atual
Ao inserir uma nova movimentação de ativos, acredito que o ideal é que não seja possível escolher uma data posterior a data atual (hoje).

## 3 - Conteinerizar o projeto para trabalho em equipe
Tentei utilizar o Docker com o Laradock, no entanto estava acontecendo muitos erros no linux de um dos containers, logo ao ver que estava perdendo
muito tempo tentando resolver erros de execução, acabei optando por continuar o desenvolvimento utilizando instalações individuais diretamente no windows (PHP 7.4, composer, mysql).

## 4 - Criação de múltiplas carteiras por cliente
Acredito que isso não seja tão utilizado ou necessário, porém o modelo do banco já está modelado para suportar mais de um portfólio de ativos por cliente.

## 5 - Importação de dados utilizando chave numérica no lugar do nome

Como está sendo feita, pode acontecer de dois clientes terem o mesmo nome, ou então existir o cliente no BD mas o nome no arquivo estar escrito de maneira diferente.

## 6 - Implementar testes de unidade:
- Importação de arquivos com até 10000 movimentações;
- Criação em lote de 3000 clientes com 150000 movimentações;

## 7 - Paginação na listagem de movimentações do cliente
Como a listagem de movimentações tende a ser muito grande, o ideal é que ela seja apresentada aos poucos, dessa forma não sobrecarrega a aplicação e o navegador. 

Sugestão: A listagem pode ser feita com botões (avançar e voltar) ou auto carregável na rolagem da página.



